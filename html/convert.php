<?php
$fpr = fopen("movie_ngt.txt","r");
$fpwn = fopen("movie_ngt_index.txt","w");
$fpwc = fopen("movie_ngt_contents.txt","w");
while($line = trim(fgets($fpr))){
    $data = explode("\t",$line);
    $tag = $data[0];
    $url = $data[1];
    $title = $data[2];
    $desc = $data[3];
    $vector = $data[4];
    if(empty($vector)){
        continue;
    }
    fwrite($fpwc,$tag."\t".$url."\t".$title."\t".$desc."\n");
    $vector = preg_replace("/^\[/","",$vector);
    $vector = preg_replace("/\]$/","",$vector);
    $vector = preg_replace("/  */"," ",$vector);
    fwrite($fpwn,$vector."\n");
}
fclose($fpr);
fclose($fpwn);
fclose($fpwc);
?>
