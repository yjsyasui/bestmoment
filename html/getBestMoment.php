<?php
require_once("./lib/Util.php");
$query = strtolower($_GET["q"]);
$now = time();
$dbh = openDB();
$querys = explode(" ",$query);
$vecs = array();
for($i=0;$i<200;$i++){
    $vecs[$i]=0;
}
foreach($querys as $q){
    $sql = "select query,vector from querys where query='".$q."'";
    $st = $dbh->prepare($sql);
    $st->execute();
    $row = $st->fetch();
    $vec = explode(",",$row['vector']);
    for($i=0;$i<5;$i++){
        $vecs[$i]+=$vec[$i];
    }
}
if($vecs[0]== 0 && $vecs[1] == 0){
    $output = array(
            "status"=>"no match"
            );
    $json = json_encode($output);
    header("Content-Type: application/json; charset=utf-8");
    header("Access-Control-Allow-Origin:*");
    print($json);
    exit(0);
}
$ngt_query = implode(" ",$vecs);
$fp_ngtq = fopen("./ngt/query_".$now.".txt","w");
fwrite($fp_ngtq,$ngt_query."\n");
fclose($fp_ngtq);
/* 　ここからNGT作ってから　*/
exec("ngt search -n 5 movie_index ./ngt/query_".$now.".txt",$result);
$movierank = getIdFromNGT($result);
exec("ngt search -n 5 recipe_index ./ngt/query_".$now.".txt",$result);
$reciperank = getIdFromNGT($result);
$best_movie_id = $movierank[0]['id'];
$best_recipe_id = $reciperank[0]['id'];

$fp_movie = fopen("./moviedata.tsv","r");
$fp_recipe = fopen("./recipedata.tsv","r");
$num = 1;
while($line = trim(fgets($fp_movie))){
    $data = explode("\t",$line);
    if($num == $best_movie_id){
        $movie_tag = $data[0];
        $movie_title = $data[2];
        $movie_url = $data[1];
        $movie_desc = $data[3];
    }
    $num++;
}
$num = 1;
while($line = trim(fgets($fp_recipe))){
    $data = explode("\t",$line);
    $id = $data[0];
    if($num == $best_recipe_id){
        $recipe_tag = $data[0];
        $recipe_title = $data[2];
        $recipe_url = $data[1];
        $recipe_desc = $data[3];
    }
    $num++;
}
fclose($fp_movie);
fclose($fp_recipe);
$output = array(
        "status"=>"success",
        "recipe"=>array(
            "tag"=>$recipe_tag,
            "title"=>$recipe_title,
            "url"=>$recipe_url,
            "description"=>$recipe_desc
            ),
        "movie"=>array(
            "tag"=>$movie_tag,
            "title"=>$movie_title,
            "url"=>$movie_url,
            "description"=>$movie_desc
            )
        );
/* 　ここまで作ってから　*/
$output = array(
        "status"=>"success",
        "recipe"=>array(
            "tag"=>"new-year/appetizers/",
            "title"=>"Bacon Wrapped Dates Stuffed with Blue Cheese",
            "url"=>"http://allrecipes.com/recipe/75417/bacon-wrapped-dates-stuffe",
            "description"=>"Dates are stuf     fed with blue cheese, wrapped in bacon and baked until crisp. These are delicious and      very easy to make for a party. You can serve them at room temperature, so it is okay      to make a few hours in advance!"
            ),
        "movie"=>array(
            "tag"=>"90's-themed",
            "title"=>"In the Mood for Love",
            "url"=>"http://www.imdb.com/title/tt0118694/",
            "description"=>"A beautifully crafted film about the unrequited love of two people renting adjacent rooms in a Shanghaiese territory in Hong Kong in the 1960s. The main characters, played by Tony Leung and Maggie Cheung, struggle to stay true to their values rather than give in to their desires although they both suspect their spouses of extramarital activities. The flawless acting and stunning visuals and editing wonderfully represent the melancholia of repressed emotions, and help the audience truly “feel” the film’s “mood”."
            )
        );
$json = json_encode($output);
header("Content-Type: application/json; charset=utf-8");
print($json);
$cmd = exec("rm ./ngt/query_".$now.".txt");

function getIdFromNGT($ngts){
    $dists = array();
    $flg = 0;
    foreach($ngts as $line){
        if($line == "Rank\tIN-ID\tID\tDistance"){
            $flg = 1;
            continue;
        }
        if($flg == 1){
            if(preg_match("/^Query Time/",$line)==1){
                break;
            }
            $tmp = array();
            $tmp['id'] = $data[1];
            $tmp['dist'] = $data[2];
            $dists[] = $tmp;
        }
    }
    return $dists;
}
?>
