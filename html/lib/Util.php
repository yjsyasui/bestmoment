<?php

function openDB(){
    $dsn = sprintf(
        'mysql:dbname=%s;host=%s',
        "bestmoment",
        "localhost"
    );
    $user = "ec2";
    $pass = "mysql";
    try {
        $dbh = new PDO($dsn,$user);
    } catch (PDOException $e){
        print('Error:'.$e->getMessage());
        return false;
    }
    return $dbh;
}
?>
