package corpusCrawler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by wchen on 12/14/16.
 */
public class AllRecipesCrawler {
    public String Crawler(String urlStr) {
        URL url;
        try {
            url = new URL(urlStr);
            System.out.println(url.toString());
        } catch(Exception e) {
            System.err.println(e.getMessage());
            throw new RuntimeException(e);
        }
        URLConnection connection;
        StringBuilder builder = new StringBuilder();
        String builderStr;
        String line;
        BufferedReader reader = null;
        try {
            connection = url.openConnection();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
            while((line = reader.readLine()) != null) {
                builder.append(line);
            }
            builderStr = builder.toString();
            return builderStr;
        } catch(Exception e1) {
            System.err.println(e1);
            return "<h1>Bummer.</h1>";
        } finally {
            try {
                reader.close();
            } catch(Exception e) {
                System.err.println(e);
                return "<h1>Bummer.</h1>";
            }
        }

    }
}
