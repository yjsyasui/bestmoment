package Html2Json;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;

/**
 * Created by wchen on 12/14/16.
 */
public class CategHtml2Json {
    public org.jsoup.nodes.Document Doc;
    public File inPut;

    public void SetPath(String Path) {

        inPut = new File(Path);
    }

    public org.jsoup.nodes.Document Parser() throws IOException {

        Doc = Jsoup.parse(inPut, "UTF-8", "http://");

        return Doc;

    }
    public Document Parser(String html){
        Doc = Jsoup.parse(html);
        return Doc;
    }

}
