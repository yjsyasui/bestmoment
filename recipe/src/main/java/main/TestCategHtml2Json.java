package main;

import Html2Json.CategHtml2Json;
import bean.FIlePath;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import utils.FileOperator;
import utils.FolderOperator;
import utils.IOhandle;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by wchen on 12/14/16.
 */
public class TestCategHtml2Json {
    public static void main(String[] args) throws IOException {
        CategHtml2Json categHtml2Json = new CategHtml2Json();
        FolderOperator folderOperator = new FolderOperator();
        String folderPath = FIlePath.FOLDERPATH;
        List<File> filesList = folderOperator.ReadAllFile(folderPath);
        FileOperator.deleteFile(folderPath + "CategoryForRecipe/CategoryForRecipe.tsv");
        BufferedWriter bwAppend = IOhandle.fileWriterContinue(folderPath + "CategoryForRecipe/CategoryForRecipe.tsv");

        for(File file : filesList) {
            String filePath = file.getAbsolutePath();
            categHtml2Json.SetPath(filePath);
            Document doc = categHtml2Json.Parser();
            // get categories info
            String canonicalUrl = "";
            try {
                canonicalUrl = doc.getElementById("canonicalUrl").attr("href");
            } catch(Exception e) {
                continue;
            }
            if(!canonicalUrl.contains("holidays-and-events")) {
                continue;
            }
            String category = "";

            if(canonicalUrl.contains("/?")) {
                String[] urlSplit = canonicalUrl.split("/?");
                String urlhead = urlSplit[0];
                String[] urlHeadSplit = urlhead.split("/");
                StringBuffer sb = new StringBuffer();
                for(int i = 6; i < urlHeadSplit.length; i++) {
                    sb.append(urlHeadSplit[i] + "/");
                }
                category = sb.toString();
            } else {
                String[] urlSplit = canonicalUrl.split("http://allrecipes.com/recipes/");
                String urlTail = urlSplit[1];
                String[] urlHeadSplit = urlTail.split("/");
                StringBuffer sb = new StringBuffer();
                for(int i = 2; i < urlHeadSplit.length; i++) {
                    sb.append(urlHeadSplit[i] + "/");
                }
                category = sb.toString();

            }

            // get recipe overview info
            Elements articleList = doc.getElementsByAttributeValue("class", "grid-col--fixed-tiles");
            for(Element article : articleList) {
                StringBuffer sbResult = new StringBuffer();

                try {
                    String recipeUrl = "http://allrecipes.com" + article.select("a").get(0).attr("href");
                    String recipeName = article.select("h3").get(0).text();
                    String recipeDesc = article.getElementsByClass("rec-card__description").text();
                    String recipeRate = article.getElementsByClass("rating-stars").attr("data-ratingstars");
                    sbResult.append(category + "\t" + recipeUrl + "\t" + recipeRate + "\t" + recipeName + "\t" + recipeDesc);
                    bwAppend.write(sbResult.toString());
                    bwAppend.newLine();
                    bwAppend.flush();
                } catch(Exception e) {
                    System.out.println(article.toString());
                }
            }

        }
        bwAppend.close();
    }

}
