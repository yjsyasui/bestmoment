package main;

import corpusCrawler.AllRecipesCrawler;
import utils.IOhandle;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * Created by wchen on 12/14/16.
 */
public class TestAllRecipesCrawler {
    public static void main(String[] args) throws IOException {
        AllRecipesCrawler allRecipesCrawler = new AllRecipesCrawler();
        String ouputFolder = "/Users/wchen/Documents/Eclipseworkspace/Hackday_JoyMaker/Result/allRecipes/";
        String urlRoot = "http://allrecipes.com/recipes/";
        for(int i = 14214; i < 50000; i++) {
            String url = urlRoot + Integer.toString(i) + "/";
            String html = allRecipesCrawler.Crawler(url);
            if(!html.contains("<h1>Bummer.</h1>")&&!html.contains("hub-daughters__container")&&html.contains("Holidays and Events")) {
                System.out.println("get no." + i);
                BufferedWriter bw = IOhandle.fileWriter(ouputFolder + Integer.toString(i) + ".html");
                bw.write(html);
            }
        }
    }
}
