package main;

import bean.FIlePath;
import utils.IOhandle;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by wchen on 12/15/16.
 */
public class RankByScoreForRecipe {
    public static void main(String[] args) throws IOException {
        String folderPath = FIlePath.FOLDERPATH;
        String inputPath = folderPath + "CategoryForRecipe/CategoryForRecipeWithReview.tsv";
        BufferedReader br = IOhandle.fileReader(folderPath + "CategoryForRecipe/CategoryForRecipeWithReview.tsv");
        String readLine = "";
        TreeMap<Float, String> itemMap = new TreeMap<>();
        BufferedWriter bw = IOhandle.fileWriter(folderPath + "CategoryForRecipe/CategoryForRecipeWithReviewTop1.tsv");
        while((readLine = br.readLine()) != null) {
            String[] readLineSplit = readLine.split("\t");
            Float score = Float.parseFloat(readLineSplit[2]);
            itemMap.put(score, readLine);
        }
        itemMap = (TreeMap<Float, String>) sortMapByKey(itemMap);
        String leftTag = "";
        String rightTag = "";
        Iterator it = itemMap.keySet().iterator();
        ArrayList<String> resultList = new ArrayList<>();
        while(it.hasNext()) {
            Float key = (Float) it.next();
            String[] itemSplit = itemMap.get(key).split("\t");
            rightTag = itemSplit[0];
            if(!rightTag.equals(leftTag)) {
                resultList.add(itemMap.get(key));
                leftTag = rightTag;
            }

        }
        for(String item : resultList) {
            bw.write(item);
            bw.newLine();
            bw.flush();
        }

    }

    public static Map<Float, String> sortMapByKey(Map<Float, String> map) {
        if(map == null || map.isEmpty()) {
            return null;
        }

        Map<Float, String> sortMap = new TreeMap<Float, String>(new MapKeyComparator());

        sortMap.putAll(map);

        return sortMap;
    }

}

class MapKeyComparator implements Comparator<Float> {

    @Override
    public int compare(Float f1, Float f2) {

        return f1.compareTo(f2);
    }
}
