package main;

import Html2Json.CategHtml2Json;
import bean.FIlePath;
import corpusCrawler.AllRecipesCrawler;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import utils.FileOperator;
import utils.IOhandle;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * Created by wchen on 12/14/16.
 */
public class TestGetReviewForRecipe {
    public static void main(String[] args) throws IOException {
        String folderPath = FIlePath.FOLDERPATH;
        BufferedReader br = IOhandle.fileReader(folderPath + "CategoryForRecipe/CategoryForRecipe.tsv");
        FileOperator.deleteFile(folderPath + "CategoryForRecipe/CategoryForRecipeWithReview.tsv");
        BufferedWriter bwAppend = IOhandle.fileWriterContinue(folderPath + "CategoryForRecipe/CategoryForRecipeWithReview.tsv");

        String readLine = "";
        AllRecipesCrawler allRecipesCrawler = new AllRecipesCrawler();
        CategHtml2Json categHtml2Json = new CategHtml2Json();
        while((readLine = br.readLine()) != null) {
            String[] readLineSplit = readLine.split("\t");
            String category = readLineSplit[0];
            String url = readLineSplit[1];
            String title = readLineSplit[2];
            String description = readLineSplit[3];
            // get html for recipes by url
            String html = allRecipesCrawler.Crawler(url);
            Document document = categHtml2Json.Parser(html);
            //parse recipe html for reviews
            Element reviewBlock = document.getElementById("reviews");
            Elements reviews;
            try {
                reviews = reviewBlock.getElementsByAttributeValue("itemprop", "reviewBody");
            } catch(Exception e) {
                continue;
            }
            StringBuffer sb = new StringBuffer();
            for(Element review : reviews) {
                String reviewStr = review.text();
                sb.append(reviewStr + "\t");
            }
            bwAppend.write(readLine + "\t" + sb.toString());
            bwAppend.newLine();
            bwAppend.flush();
        }
        bwAppend.close();
    }
}
