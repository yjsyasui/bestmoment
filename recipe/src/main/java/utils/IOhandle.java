/**
 *
 */
package utils;

import java.io.*;

/**
 * @author wchen
 * @create-time Jul 2, 2013   12:32:02 PM
 */
public class IOhandle {

    public static BufferedReader fileReader(String InputPath) throws IOException {
        FileReader fr = new FileReader(InputPath);
        BufferedReader br = new BufferedReader(fr);
        return br;
    }

    public static BufferedWriter fileWriter(String OutputPath) throws IOException {
        FileWriter fw = new FileWriter(OutputPath);
        BufferedWriter bw = new BufferedWriter(fw);
        return bw;
    }

    public static BufferedWriter fileWriterContinue(String OutputPath) throws IOException {
        FileWriter fw = new FileWriter(OutputPath, true);
        BufferedWriter bw = new BufferedWriter(fw);
        return bw;
    }
}
