package utils;

import java.io.File;

public class FileOperator {

    public static boolean delete(String fileName) {
        File file = new File(fileName);
        if(!file.exists()) {
            System.out.println("failure：" + fileName + " not exist in the path");
            return false;
        } else {
            if(file.isFile()) {

                return deleteFile(fileName);
            } else {
                return deleteDirectory(fileName);
            }
        }
    }

    /**
     * delete single file
     *
     * @param fileName
     * @return to be successful and return 1, otherwise return 0
     */
    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        if(file.isFile() && file.exists()) {
            file.delete();
            System.out.println("delete " + fileName + " successful");
            return true;
        } else {
            System.out.println("failure：" + fileName + " not exist in the path");
            return false;
        }
    }

    /**
     * delete the folder and all the files in it
     *
     * @param dir
     * @return to be successful and return 1, otherwise return 0
     */
    public static boolean deleteDirectory(String dir) {
        if(!dir.endsWith(File.separator)) {
            dir = dir + File.separator;
        }
        File dirFile = new File(dir);
        // if targer folder not exists, exit
        if(!dirFile.exists() || !dirFile.isDirectory()) {
            System.out.println("failure：" + dir + " not exist in the path");
            return false;
        }
        boolean flag = true;
        //delete the folder and all the files or sub-folder in it
        File[] files = dirFile.listFiles();
        for(int i = 0; i < files.length; i++) {
            //delete files
            if(files[i].isFile()) {
                flag = deleteFile(files[i].getAbsolutePath());
                if(!flag) {
                    break;
                }
            }
            //delete sub-folders
            else {
                flag = deleteDirectory(files[i].getAbsolutePath());
                if(!flag) {
                    break;
                }
            }
        }

        if(!flag) {
            System.out.println("failure");
            return false;
        }

        //delete current folder
        if(dirFile.delete()) {
            System.out.println("delete " + dir + " successful");
            return true;
        } else {
            System.out.println("failure：" + dir + " not exist in the path");
            return false;
        }
    }

}